package ro.upt.socialsecuritynumber;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.net.ConnectException;

public class MainActivity extends AppCompatActivity {

  private TextInputLayout tilSsnInput;
  private Button btSsnSubmit;
  private TextView tvSsnResult;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.view_main);

    tilSsnInput = findViewById(R.id.til_ssn_input);
    btSsnSubmit = findViewById(R.id.bt_ssn_submit);
    tvSsnResult = findViewById(R.id.tv_ssn_result);

    btSsnSubmit.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        String input = tilSsnInput.getEditText().getText().toString();
        if (input.isEmpty() || input.length() < 16) {
          tilSsnInput.setError(getString(R.string.ssn_too_short));
          return;
        }
        try {
          String result = SocialSecurityNumberService.check(input);
          if (result.isEmpty()) {
            tvSsnResult.setText(R.string.ssn_not_found);
          } else {
            tvSsnResult.setText(result);
          }
        } catch (ConnectException e) {
          Toast.makeText(MainActivity.this, R.string.no_network_connection, Toast.LENGTH_LONG)
              .show();
        }
      }
    });
  }
}
