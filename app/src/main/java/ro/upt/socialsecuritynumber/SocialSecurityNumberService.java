package ro.upt.socialsecuritynumber;

import java.net.ConnectException;
import java.util.Random;

class SocialSecurityNumberService {

  static String check(String ssn) throws ConnectException {
    boolean generateNetworkError = new Random().nextBoolean();
    if (generateNetworkError) {
      throw new ConnectException();
    }

    boolean found = new Random().nextBoolean();
    if (found) {
      return "Donald Prumpt";
    }

    return "";
  }

}
